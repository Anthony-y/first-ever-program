﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Synthesis;

namespace Lift_Simulator
{
    class Lift
    {
        static int currentFloor;
        static int wantedFloor;
        static int newCurrentFloor;
        static SpeechSynthesizer Elevator = new SpeechSynthesizer();

        static void Say(string str)
        {
            Elevator.Speak(str);
            Console.WriteLine(str + "\n");
        }

        static void Main(string[] args)
        {
            while (true)
            {
                Elevator.SelectVoiceByHints(VoiceGender.Female);

                Say("Which floor?");
                
                wantedFloor = Convert.ToInt32(Console.ReadLine());
                if (wantedFloor <= 0)
                {
                    Say("You are on the ground floor.");
                    Say("You may only go up.");
                    Console.Clear();
                    continue;
                }
                else
                {
                    currentFloor = wantedFloor;
                    Say("You are now on floor " + currentFloor);
                    Console.WriteLine("Would you like to go down or up?\n");
                    string upOrDown = Console.ReadLine();
                    if (upOrDown == "up" || upOrDown == "u")
                    {
                        Elevator.SpeakAsync("Going up.");
                        Elevator.Pause();
                        Elevator.Resume();
                        newCurrentFloor = currentFloor + 1;
                        Say("You are now on floor " + newCurrentFloor);
                        Console.Clear();
                    }
                    else if (upOrDown == "down" || upOrDown == "d")
                    {
                        Elevator.SpeakAsync("Going down.");
                        Elevator.Pause();
                        Elevator.Resume();
                        newCurrentFloor = currentFloor - 1;
                        Console.WriteLine("You are now on floor " + newCurrentFloor + "\n");
                        Console.Clear();
                    }
                    else if (upOrDown == "Funky town")
                    {
                        Elevator.SpeakAsync("Going down to, Funky town.");
                    }
                    else
                    {
                        Elevator.SpeakAsync("I don't understand what you mean.");
                        Console.Clear();
                    }
                }
                
                Console.Clear();
            }   
        }
    }
}